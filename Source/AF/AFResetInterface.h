// implement this interface for Actors that should handle game mode resets
#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "UObject/Interface.h"
#include "AFResetInterface.generated.h"

UINTERFACE(MinimalAPI)
class UAFResetInterface : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

//class ZB4_API IZB4ResetInterface
class IAFResetInterface
{
	GENERATED_IINTERFACE_BODY()

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = Game)
	void Reset();
};