#pragma once
#include "AFTypes.generated.h"

USTRUCT(BlueprintType)
struct FFxSetup
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FXParameter")
		UParticleSystem* ParticleEffect;
	/** Actor to spawn/attached when the FX is activated. An actor can contain light/fx/mesh
	so it can handle complex things or just contains SKMesh / StaticMesh */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FXParameter")
		TSubclassOf<AActor> ActorClassToSpawn;
	/** Relative transform to spawn the SKMesh/StaticMesh from spawn location */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FXParameter")
		FTransform SpawnRelativeTransform;
	/** name of bone/socket for effect on owner mesh. Only used when AttachToOwner is involved.*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FXParameter")
		FName FXAttachSocket;
	/** name of bone/socket for SKMesh/StaticMesh/Actor on owner mesh. Only used when AttachToOwner is involved.*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FXParameter")
		FName MeshAttachSocket;
	
};

