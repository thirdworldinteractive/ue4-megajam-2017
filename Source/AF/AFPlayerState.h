#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "AFPlayerState.generated.h"

UCLASS(Blueprintable, Config = Game)
class AAFPlayerState : public APlayerState
{
	GENERATED_BODY()
	
public:
	AAFPlayerState();
	
	/** Update Player score*/
	void AdjustScore(int32 ScoreAdjustment);
	
	UFUNCTION(BlueprintCallable, Category=Game)
	float GetLightEnergy() const;

	UFUNCTION(BlueprintCallable, Category=Game)
	int32 GetScoreMultiplier() const;
	
	UFUNCTION(BlueprintCallable, Category = Game)
		void AddLightEnergy(float Quantity);

	/** Get a value between 1 to 0 for the timer progression */
	UFUNCTION(BlueprintCallable, Category = UI)
		float GetBonusTimerValue() const;
	
	/** Pause the bonus timer. Use it during time between 2 stages / sequencer / animation... where the player is not playing*/
	UFUNCTION(BlueprintCallable, Category = Game)
	void PauseBonusTimer(bool ShouldPause);
	
private:
	float LightEnergy;
	int32 CurrentMultiplier;
	int32 MultiplierXP;	
	
protected:
	
	/** Time in second before the bonus multiplier is reseted */
	UPROPERTY(EditDefaultsOnly, Category = Game)
	int32 ScoreMultiplierTimeOut;
	
	UPROPERTY(EditDefaultsOnly, Category = Game)
	int32 MaxScoreMultiplier;
	/** Use to setup the different level of score bonus */
	UPROPERTY(EditDefaultsOnly, Category = Game)
	int32 ScoreMultiplierRatio;
	
	FTimerHandle BonusTimerHandle;
	
	void ResetBonusTimer();
	
};