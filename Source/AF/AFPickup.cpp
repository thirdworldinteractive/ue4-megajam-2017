#include "AFPickup.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundBase.h"
#include "Particles/ParticleSystemComponent.h"
#include "AFPawn.h"
#include "AF.h"


void AAFPickup::PostEditImport()
{
	Super::PostEditImport();

	if (!IsPendingKill())
	{
		Collision->OnComponentBeginOverlap.Clear();
		Collision->OnComponentBeginOverlap.AddDynamic(this, &AAFPickup::OnOverlapBegin);
	}
}

AAFPickup::AAFPickup()
{
	bCanBeDamaged = false;

	Collision = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Capsule"));
	Collision->SetCollisionProfileName(FName(TEXT("Pickup")));
	Collision->InitCapsuleSize(64.0f, 75.0f);
	Collision->bShouldUpdatePhysicsVolume = false;
	Collision->Mobility = EComponentMobility::Movable;
	Collision->OnComponentBeginOverlap.AddDynamic(this, &AAFPickup::OnOverlapBegin);
	RootComponent = Collision;

	PrimaryActorTick.bCanEverTick = true;
}

void AAFPickup::Reset_Implementation()
{
	Destroy();
}

void AAFPickup::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepHitResult)
{
	AAFPawn* P = Cast<AAFPawn>(OtherActor);
	if (P != nullptr && !P->bTearOff && !GetWorld()->LineTraceTestByChannel(P->GetActorLocation(), GetActorLocation(), ECC_Pawn, FCollisionQueryParams(), WorldResponseParams))
	{
		ProcessTouch(P);
	}
}

void AAFPickup::ProcessTouch_Implementation(APawn* TouchedBy)
{
	if (TouchedBy->Controller != nullptr)
	{
		GiveTo(TouchedBy);
		PlayEffects();
	}
}

void AAFPickup::GiveTo_Implementation(APawn* Target)
{
	if (Cast<APlayerController>(Target->GetController()))
	{
		//No Default Implementation
	}
}

void AAFPickup::PlayEffects()
{
	if (Particles != nullptr)
	{
		UParticleSystemComponent* PSC = UGameplayStatics::SpawnEmitterAttached(Particles, RootComponent, NAME_None, ParticlesEffectTransform.GetLocation(), ParticlesEffectTransform.GetRotation().Rotator());
		if (PSC != nullptr)
		{
			PSC->SetRelativeScale3D(ParticlesEffectTransform.GetScale3D());
		}
	}
	if( PickupSound != nullptr)
	{
		UGameplayStatics::PlaySoundAtLocation(this, PickupSound, GetActorLocation());
	}
	SetLifeSpan(0.25f);
}