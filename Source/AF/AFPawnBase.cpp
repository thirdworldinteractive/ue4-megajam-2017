#include "AFPawnBase.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Engine/CollisionProfile.h"
#include "Engine/StaticMesh.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundBase.h"
#include "AFGameModeHorde.h"
#include "Particles/ParticleSystemComponent.h"
#include "AF.h"

AAFPawnBase ::AAFPawnBase()
{
	// Movement
	Health = 100.f;
}

void AAFPawnBase::BeginPlay()
{
	Super::BeginPlay();
}

float AAFPawnBase::TakeDamage(float Damage, const FDamageEvent& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	if (Health <= 0.f)
	{
		return 0.f;
	}
	// else if (Damage < 0.0f)
	// {
		// UE_LOG(LogAF, Warning, TEXT("TakeDamage() called with damage %i of type %s... use HealDamage() to add Health"), int32(Damage), *GetNameSafe(DamageEvent.DamageTypeClass));
		// return 0.0f;
	// }
	
	const float ActualDamage = Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
	Health -= ActualDamage;

	if(ActualDamage >0 )
	{
		OnHit();
	}
	UE_LOG(LogAF, Verbose, TEXT("%s took %d damage, %d Health remaining"), *GetName(), ActualDamage, Health);
	if (Health <= 0)
	{
		Died(EventInstigator, DamageEvent);
	}

	return ActualDamage;
}

bool AAFPawnBase::Died(AController* EventInstigator, const FDamageEvent& DamageEvent)
{
	UE_LOG(LogAF, Verbose, TEXT("Died being called"));
	// if this is an environmental death then refer to the previous killer so that they receive credit (knocked into lava pits, etc)
	if (DamageEvent.DamageTypeClass != nullptr && DamageEvent.DamageTypeClass.GetDefaultObject()->bCausedByWorld && (EventInstigator == nullptr || EventInstigator == Controller) && LastHitBy != nullptr)
	{
		EventInstigator = LastHitBy;
	}
	
	TearOff();// important to set this as early as possible so IsDead() returns true
	GetWorld()->GetAuthGameMode<AAFGameModeHorde>()->Killed(EventInstigator, (Controller != nullptr) ? Controller : Cast<AController>(GetOwner()), this, DamageEvent.DamageTypeClass);

	Health = FMath::Max<int32>(Health, 0);

	if (Controller != nullptr)
	{
		Controller->PawnPendingDestroy(this);
	}

	PlayDying();
	UE_LOG(LogAF, Verbose, TEXT("OnDied being called"));
	OnDied();
	return true;
}

void AAFPawnBase::PlayDying()
{
	UE_LOG(LogAF, Verbose, TEXT("PlayDying being called"));
	if (!IsPendingKillPending())
		SetLifeSpan(0.25f);

	// Create the activating particle template that everyone can see
	if (EliminationFxSetup.ParticleEffect)
	{
		UParticleSystemComponent* EliminationPSC;
		EliminationPSC = UGameplayStatics::SpawnEmitterAtLocation(this, EliminationFxSetup.ParticleEffect, GetActorLocation(), GetActorRotation());
	}
	
	if (EliminationFxSetup.ActorClassToSpawn != nullptr)
	{
		AActor* SpawnedActor = UGameplayStatics::BeginDeferredActorSpawnFromClass(
			this,
			EliminationFxSetup.ActorClassToSpawn,
			GetTransform() + EliminationFxSetup.SpawnRelativeTransform,
			ESpawnActorCollisionHandlingMethod::AlwaysSpawn,
			this);
		SpawnedActor->SetLifeSpan(0.5f);
		UGameplayStatics::FinishSpawningActor(SpawnedActor, GetTransform() + EliminationFxSetup.SpawnRelativeTransform);

		if (SpawnedActor != nullptr)
		{
			TInlineComponentArray<UParticleSystemComponent*> PscComponents;
			SpawnedActor->GetComponents<UParticleSystemComponent>(PscComponents);
		}
	}
}