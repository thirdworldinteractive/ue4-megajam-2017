#pragma once

#include "GameFramework/Pawn.h"
#include "Components/CapsuleComponent.h"
#include "AFResetInterface.h"
#include "AFPickup.generated.h"

UCLASS(Blueprintable, abstract, meta = (ChildCanTick))
class AAFPickup : public AActor, public IAFResetInterface
{
	GENERATED_BODY()
	
public:
	AAFPickup();

	virtual void PostEditImport() override;

	UFUNCTION()
	virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepHitResult);

	virtual void Reset_Implementation() override;

	UFUNCTION(BlueprintNativeEvent)
	void ProcessTouch(APawn* TouchedBy);

	UFUNCTION(BlueprintNativeEvent, BlueprintAuthorityOnly)
	void GiveTo(APawn* Target);

	UFUNCTION(BlueprintCallable, Category = Pickup)
	virtual void PlayEffects();

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Pickup)
	UCapsuleComponent* Collision;

	/** one-shot particle effect played when the pickup is taken */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Effects)
	UParticleSystem* Particles;
	/** Transform from position of the pickup where the particules will be spawned */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Effects)
	FTransform ParticlesEffectTransform;
	
	/** one-shot sound played when the pickup is taken */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Effects)
	USoundBase* PickupSound;


};