#include "AFPlayerState.h"
#include "AF.h"

AAFPlayerState::AAFPlayerState()
{
	Score = 0;
	LightEnergy = 0;
	CurrentMultiplier = 1;
	MaxScoreMultiplier = 4;
	ScoreMultiplierRatio = 100;
	ScoreMultiplierTimeOut = 5;
	
}

void AAFPlayerState::AdjustScore(int32 ScoreAdjustment)
{
	Score += ScoreAdjustment * CurrentMultiplier;
	if (ScoreAdjustment >0)
	{
		//Increment the Mutiplier
		MultiplierXP +=ScoreAdjustment;
		//Calculater the multiplier
		int32 base = FPlatformMath::FloorToInt(MultiplierXP / ScoreMultiplierRatio);
		CurrentMultiplier = FPlatformMath::FloorToInt(FMath::Sqrt(base));
		
		//Clamp to MaxScoreMultiplier
		CurrentMultiplier =FMath::Max<int32>(1, FMath::Min<int32>(CurrentMultiplier, MaxScoreMultiplier));
		
		//Reset Bonus Timer
		GetWorldTimerManager().SetTimer(BonusTimerHandle,this, &AAFPlayerState::ResetBonusTimer,ScoreMultiplierTimeOut,false);
	}
}

void AAFPlayerState::ResetBonusTimer()
{
	CurrentMultiplier = 1;
	MultiplierXP = 0;
}

void AAFPlayerState::PauseBonusTimer(bool ShouldPause)
{
	if (BonusTimerHandle.IsValid())
	{
		if (ShouldPause)
		{
			GetWorldTimerManager().PauseTimer(BonusTimerHandle);
		}
		else
		{
			GetWorldTimerManager().UnPauseTimer(BonusTimerHandle);
		}
	}
}
float AAFPlayerState::GetBonusTimerValue() const
{
	if (BonusTimerHandle.IsValid() && GetWorldTimerManager().IsTimerActive(BonusTimerHandle))
	{
		return GetWorldTimerManager().GetTimerRemaining(BonusTimerHandle)/ScoreMultiplierTimeOut;
	}
	return 0.f;
}

float AAFPlayerState::GetLightEnergy() const
{
	return LightEnergy;
}

void AAFPlayerState::AddLightEnergy(float Quantity)
{
	LightEnergy += Quantity;
}

int32 AAFPlayerState::GetScoreMultiplier() const
{
	return CurrentMultiplier;
}