// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

DECLARE_LOG_CATEGORY_EXTERN(LogAF, Log, All);

/** handy object query params for world-only checks */
extern FCollisionResponseParams WorldResponseParams;