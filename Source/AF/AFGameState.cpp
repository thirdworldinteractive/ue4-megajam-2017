#include "AFGameState.h"
#include "AF.h"

AAFGameState::AAFGameState()
{
	SetTimeLimit(60);
	SetGoalScore(50);
}

void AAFGameState::DefaultTimer()
{
	Super::DefaultTimer();

	if ( RemainingTime > 0 && IsMatchInProgress() )
	{
		RemainingTime--;
	}	
}

void AAFGameState::SetTimeLimit(int32 NewTimeLimit)
{
	UE_LOG(LogAF, Log, TEXT("AAFGameState.SetTimeLimit %i"), NewTimeLimit);
	TimeLimit = NewTimeLimit;
	RemainingTime = TimeLimit;
}

void AAFGameState::SetGoalScore(int32 NewGoalScore)
{
	GoalScore = NewGoalScore;
}
