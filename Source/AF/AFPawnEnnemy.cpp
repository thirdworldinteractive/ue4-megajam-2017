#include "AFPawnEnnemy.h"
#include "Components/CapsuleComponent.h"

AAFPawnEnnemy::AAFPawnEnnemy()
{
	GetCapsuleComponent()->InitCapsuleSize(50.f, 50.0f);
	DieScore = 1;
}