#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "AFPawnBase.h"
#include "AFPawnEnnemy.generated.h"

UCLASS(Blueprintable)
class AAFPawnEnnemy : public AAFPawnBase
{
	GENERATED_BODY()
	
public:
	AAFPawnEnnemy();

	/** Set on AI for the player to score when the AI is dead*/
	UPROPERTY(EditDefaultsOnly, Category = "Game")
		int32 DieScore;
};