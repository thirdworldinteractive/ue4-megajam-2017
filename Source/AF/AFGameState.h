#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameState.h"
#include "AFGameState.generated.h"

UCLASS(Blueprintable, Config = Game)
class AAFGameState : public AGameState
{
	GENERATED_BODY()
	
public:
	AAFGameState();
	
	/** If a single player's (or team's) score hits this limited, the game is over */
	UPROPERTY( EditDefaultsOnly, BlueprintReadOnly, Category = GameState)
	int32 GoalScore;

	/** The maximum amount of time the game will be */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = GameState)
	int32 TimeLimit;
	
	/** How much time is remaining in this match. */
	UPROPERTY(BlueprintReadOnly, Category = GameState)
	int32 RemainingTime;
	
	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly, Category = GameState)
	virtual void SetTimeLimit(int32 NewTimeLimit);

	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly, Category = GameState)
	virtual void SetGoalScore(int32 NewGoalScore);
	
	/** Called once per second (or so depending on TimeDilation) */
	virtual void DefaultTimer() override;
};