// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AFGameModeBase.h"
#include "AFGameState.h"
#include "AFPlayerState.h"
#include "AFGameModeHorde.generated.h"

/**
 *
 */
UCLASS(Blueprintable)
class AAFGameModeHorde : public AAFGameModeBase
{
	GENERATED_BODY()

public:

	/** Score needed to win the match.*/
	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
		int32 GoalScore;

	AAFGameModeHorde();
	/** Cached reference to our game state for quick access. */
	UPROPERTY()
		AAFGameState* AFGameState;
	virtual void InitGameState() override;

	/** Call when a pawn is killed. Manage score changes if any. */
	virtual void Killed(class AController* Killer, class AController* KilledPlayer, class APawn* KilledPawn, TSubclassOf<UDamageType> DamageType);

	/** Check if the PS reach the goal score */
	UFUNCTION(BlueprintCallable, category = UI)
	bool CheckScore(AAFPlayerState* Scorer);

	/** Use to set different value before ending the match and can be use to start a "end game" UI change / FX / sound */
	UFUNCTION(BlueprintCallable, category = Gameplay)
	virtual void EndGame(AAFPlayerState* Winner, FName Reason);

	/** Should be called by GameState when Time limit is reached */
	void TimeLimit();

	UFUNCTION(BlueprintCallable, category = UI)
		int32 GetGoalScore() const;

	/** Called when the game ended so the BP designer can call HUD changes */
	UFUNCTION(BlueprintImplementableEvent, Category = "UI")
		void OnGameEnd(FName Reason);

	/** Called when the game started so the BP designer can call HUD changes */
	UFUNCTION(BlueprintImplementableEvent, Category = "UI")
		void OnGameStart();

	/** Call this if the Wave end. That's means the player didn't catch enough light so the game is over*/
	UFUNCTION(BlueprintCallable, category = gameplay)
		void EndWave();

	virtual bool ReadyToStartMatch_Implementation() override;

	virtual void StartMatch() override;

};
