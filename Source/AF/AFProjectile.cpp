#include "AFProjectile.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Engine/StaticMesh.h"
#include "GameFramework/DamageType.h"
#include "AFPawnBase.h"

AAFProjectile::AAFProjectile() 
{
	Collision = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Capsule"));
	Collision->SetCollisionProfileName(FName(TEXT("Projectile")));
	Collision->InitCapsuleSize(64.0f, 75.0f);
	Collision->bShouldUpdatePhysicsVolume = false;
	Collision->Mobility = EComponentMobility::Movable;
	//Collision->OnComponentBeginOverlap.AddDynamic(this, &AAFPickup::OnOverlapBegin);
	Collision->OnComponentHit.AddDynamic(this, &AAFProjectile::OnHit);
	RootComponent = Collision;

	// Use a ProjectileMovementComponent to govern this projectile's movement
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovement0"));
	ProjectileMovement->UpdatedComponent = Collision;
	ProjectileMovement->InitialSpeed = 3000.f;
	ProjectileMovement->MaxSpeed = 3000.f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = false;
	ProjectileMovement->ProjectileGravityScale = 0.f; // No gravity

	Damage = 10.f;
	// Die after 3 seconds by default
	InitialLifeSpan = 3.0f;
}

void AAFProjectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	AAFPawnBase* Pawn = Cast<AAFPawnBase>(OtherActor);
	if ((Pawn != nullptr) && (OtherActor != this))
	{
		APawn* Instigator = GetInstigator();
		AController* InstigatorCtrl = nullptr;
		if (Instigator != nullptr)
		{
			InstigatorCtrl = Instigator->GetController();
		}
		Pawn->TakeDamage(Damage, FDamageEvent(UDamageType::StaticClass()), InstigatorCtrl, this);
	}

	Destroy();
}