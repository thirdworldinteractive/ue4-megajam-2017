#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "AFProjectile.generated.h"

class UProjectileMovementComponent;
class UCapsuleComponent;

UCLASS(config=Game)
class AAFProjectile : public AActor
{
	GENERATED_BODY()
		
	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	float Damage;

public:
	AAFProjectile();

	/** Function to handle the projectile hitting something */
	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	/** Returns ProjectileMovement subobject **/
	FORCEINLINE UProjectileMovementComponent* GetProjectileMovement() const { return ProjectileMovement; }

protected:
	/** Capsule collision component */
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Gameplay)
		UCapsuleComponent* Collision;

	/** Projectile movement component */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Movement)
		UProjectileMovementComponent* ProjectileMovement;
};

