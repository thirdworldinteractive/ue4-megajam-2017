#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "AFTypes.h"
#include "AFPawnBase.generated.h"

UCLASS(Blueprintable)
class AAFPawnBase : public ACharacter
{
	GENERATED_BODY()

public:
	AAFPawnBase();

	virtual void BeginPlay() override;
	
	/* The fish health */
	UPROPERTY(Category = "Gameplay", EditAnywhere, BlueprintReadWrite)
		float Health;
		
	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly, Category = Pawn)
		virtual bool Died(AController* EventInstigator, const FDamageEvent& DamageEvent);
	
	virtual float TakeDamage(float Damage, const FDamageEvent& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
	
	/** Called when pawn is killed */
	UFUNCTION(Category = "Gameplay", BlueprintImplementableEvent)
		void OnDied();
	
	/** Called when pawn is hit */
	UFUNCTION(BlueprintImplementableEvent, Category = "Gameplay")
		void OnHit();
protected:
	/** plays death effects; use LastTakeHitInfo to do damage-specific death effects */
	virtual void PlayDying();
	
	/** Spawn this emitter setup when dying*/
	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
		FFxSetup EliminationFxSetup;
};