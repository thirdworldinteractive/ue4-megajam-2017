#include "AFGameModeHorde.h"
#include "AFPlayerState.h"
#include "AFGameState.h"
#include "AFPawn.h"
#include "AFPawnEnnemy.h"
#include "AF.h"

AAFGameModeHorde::AAFGameModeHorde()
{
	DefaultPawnClass = AAFPawn::StaticClass();
	GameStateClass = AAFGameState::StaticClass();
	PlayerStateClass = AAFPlayerState::StaticClass();
	GoalScore = 1000;
	//HUDClass = AHUD::StaticClass();
	//PlayerControllerClass = APlayerController::StaticClass();
}
int32 AAFGameModeHorde::GetGoalScore() const
{
	if (AFGameState != nullptr)
	{
		return AFGameState->GoalScore;
	}
	return GoalScore;
}
void AAFGameModeHorde::InitGameState()
{
	Super::InitGameState();

	AFGameState = Cast<AAFGameState>(GameState);
	if (AFGameState != nullptr)
	{
		AFGameState->SetGoalScore(GoalScore);
		AFGameState->SetTimeLimit(0);
	}
	else
	{
		UE_LOG(LogAF, Error, TEXT("AFGameState is NULL %s"), *GameStateClass->GetFullName());
	}
}

void AAFGameModeHorde::TimeLimit()
{
	EndGame(nullptr, FName(TEXT("TimeLimit")));
}

void AAFGameModeHorde::Killed(AController* Killer, AController* KilledPlayer, APawn* KilledPawn, TSubclassOf<UDamageType> DamageType)
{
	AAFPlayerState* const KillerPlayerState = Killer ? Cast<AAFPlayerState>(Killer->PlayerState) : nullptr;
	AAFPlayerState* const KilledPlayerState = KilledPlayer ? Cast<AAFPlayerState>(KilledPlayer->PlayerState) : nullptr;

	UE_LOG(LogAF, Log, TEXT("Player Killed: %s killed %s"), (KillerPlayerState != nullptr ? *KillerPlayerState->PlayerName : TEXT("NULL")), (KilledPlayerState != nullptr ? *KilledPlayerState->PlayerName : TEXT("NULL")));

	if (KillerPlayerState != nullptr && KilledPawn != nullptr)
	{
		AAFPawnEnnemy* EnemyPawn = Cast<AAFPawnEnnemy>(KilledPawn);
		if (EnemyPawn != nullptr)
		{
			KillerPlayerState->AdjustScore(EnemyPawn->DieScore);
		}
	}
	if (KilledPlayer != nullptr)
	{
		AAFPawn* PlayerPawn= Cast<AAFPawn>(KilledPawn);
		if (PlayerPawn != nullptr)
		{
			//Player was killed -> End Game
			EndGame(nullptr, FName(TEXT("Killed")));
		}
	}
}

bool AAFGameModeHorde::CheckScore(AAFPlayerState* Scorer)
{
	//Check if the player reach the Goal score.
	return Scorer->GetLightEnergy() >= GoalScore;
}

void AAFGameModeHorde::EndGame(AAFPlayerState* Winner, FName Reason)
{
	EndMatch();
	OnGameEnd(Reason);
}

bool AAFGameModeHorde::ReadyToStartMatch_Implementation()
{
	
	return Super::ReadyToStartMatch_Implementation();
}

void AAFGameModeHorde::StartMatch()
{
	Super::StartMatch();
	OnGameStart();	
}

void AAFGameModeHorde::EndWave()
{
	EndGame(nullptr,FName(TEXT("EndWave")));
}
