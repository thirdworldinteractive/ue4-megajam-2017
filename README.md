This is a Unreal Engine Mega Jam Game. You can download the game here: https://drive.google.com/open?id=1Q3Ju9UB3wuSbw9B1ZRyaTC1M95V-LD6x

**This project was made using Unreal Engine 4.18.0**

**Gameplay Instructions:** 

W,A,S,D to move 
Left Mouse to bite 
Right Mouse to do a Spin Attack 
Space to Dash

**The objective is to defeat all enemies and absorb their light by eating their meat. Each wave gets harder, until a final Wave 5.**

**List of any content that was created before the jam that was included in the final submission:**

**Gamepad UMG Plugin** https://forums.unrealengine.com/community/community-content-tools-and-tutorials/58523-gamepad-friendly-umg-~-control-cursor-with-gamepad-analog-stick-easily-click-buttons
**VictoryPlugin** https://forums.unrealengine.com/development-discussion/blueprint-visual-scripting/4014-39-rama-s-extra-blueprint-nodes-for-you-as-a-plugin-no-c-required?3851-(39)-Rama-s-Extra-Blueprint-Nodes-for-You-as-a-Plugin-No-C-Required=
**Cardinal Menu** http://metahusk.com/cardinal-menu/

**LICENSE:**  As a condition of entry, Contestants hereby provide a non-exclusive irrevocable worldwide license to Sponsor to use the Submission in any and all media throughout the world for the purpose of promoting the Contest and future versions of the Contest and Sponsor’s Unreal Engine marketing, but for no other purpose, without any additional compensation, the term of which shall be the entire life of the copyright. Should any Contestant be unwilling or otherwise unable to enter into this license, or provide permissions and or releases, or otherwise cannot accept or receive the prize for any reason, Contestant with the next highest score will be chosen from the remaining entries until one who is able to meet all requirements can be selected. Potential prize winner must provide Sponsor with all signatures on required paperwork and return all documents in a timely manner as required pursuant to these Official Rules in order to be eligible to receive the prize. Contestants may not sell, assign or transfer any of their rights in their Submissions under these Official Rules.